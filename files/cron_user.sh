#!/bin/bash
set -e

CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
CO_ROLES_ENABLES=$(jq -r .co_roles_enabled < /etc/rsc/workspace.json)
CO_PASSWORDLESS_SUDO=$(jq -r .co_passwordless_sudo < /etc/rsc/workspace.json)
CO_ADMIN_USER_ONLY=$(jq -r .co_admin_user_only < /etc/rsc/workspace.json)
CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
curl -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}"
CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')
MANAGED_USERS_DIR="/etc/rsc/managedusers"

# Disable missing users
mkdir -p "${MANAGED_USERS_DIR}"
for file in "$MANAGED_USERS_DIR"/*
do
  [[ -f "${file}" ]] || break
  user=$(basename -- "${file}")
  if ! echo "${CO_USER_INFO}" | grep "\"${user}\"," &>/dev/null
  then
    /usr/sbin/usermod -s /usr/sbin/nologin "${user}" || true
    rm "${file}"
    echo "removing user ${user} from login"
  fi
done

echo "${CO_USER_INFO}" | jq -c '.[]' | while read -r user
do
    username=$(echo "${user}" | jq -r '.username')
    uid=$(echo "${user}" | jq -r '.integer_id')
    is_admin=$(echo "${user}" | jq 'select(.roles | index("src_co_admin"))')

    if [ "$CO_ADMIN_USER_ONLY" == "true" ] && [ -z "${is_admin}" ]; then
        echo Skipping "${username}". Is no co-admin user.
        [[ -f "$MANAGED_USERS_DIR"/"$username"  ]] && echo "removing user ${username} from login because they are not admin anymore" && rm "$MANAGED_USERS_DIR"/"$username" && /usr/sbin/usermod -s /usr/sbin/nologin "${username}" || true
        continue
    fi
    touch "$MANAGED_USERS_DIR"/"$username"

    # To fix Centos problem in killing user processes in logout
    mkdir -p /var/lib/systemd/linger
    touch "/var/lib/systemd/linger/${username}"

    echo "creating user ${username} if not exist (Starting)"
    # Create user or re-enable it if it was removed in the past
    if id -u "${username}" &>/dev/null
    then
      /usr/sbin/usermod -s /bin/bash "${username}"
    else
      /usr/sbin/useradd -u "${uid}" -s /bin/bash "${username}" -m
    fi
    if [ "$CO_ROLES_ENABLES" == "true" ] && [ ! -z "${is_admin}" ]; then
        gpasswd -a "${username}" wheel || true
    else
        gpasswd -d "${username}" wheel || true
    fi
    echo "creating user ${username} if not exist (Done)"

#if true, then add user to sudoers and make sudoers have root access without password while removing the user from the wheel group to prevent sudo conflict.
    if [ "$CO_PASSWORDLESS_SUDO" == "true" ]; then
        gpasswd -d "${username}" wheel || true
        gpasswd -a "${username}" sudoers || true
    else
        gpasswd -d "${username}" sudoers || true
    fi

    echo "Add all users to fuse group"
    gpasswd -a "${username}" fuse || true

    cp -u /usr/local/bin/user_mount_researchdrive.sh /home/"${username}/mount_researchdrive.sh" 2>/dev/null || :
    echo "Adding SSH keys to ${username}"
    echo "${CO_USER_INFO}" | jq --arg USERNAME "${username}" -r '.[] | select(.username == $USERNAME) | .ssh_keys[]' > /home/"${username}"/.ssh/authorized_keys
done
