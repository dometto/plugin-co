#!/bin/bash
set -e
USER="$1"
# Functions
get_credentials () {
    set -e
    rm -f "/home/${USER}/.davfs2/secrets"
    CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
    CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
    CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')

    RESEARCH_DRIVE_SECRET=$(echo "${CO_USER_INFO}" | jq --arg USERNAME "${USER}" -r '.[] | select(.username == $USERNAME) | .research_drive_secret')

    if [ -z "$RESEARCH_DRIVE_SECRET" ]
    then
       exit 1
    else
        jq -r -e '"\(.username//"") \(.token//"") \(.url//"")"' <<< "${RESEARCH_DRIVE_SECRET}"
    fi
}

create_secret () {
    set -e

    USERNAME="$1"
    PASSWORD="$2"
    URL="$3"

    if [ -n "${USERNAME}" ] && [ -n "${PASSWORD}" ]; then
        mkdir -p "/home/${USER}/.davfs2/" "/home/${USER}/researchdrive/"
        echo "${URL} ${USERNAME} ${PASSWORD}" > "/home/${USER}/.davfs2/secrets"
        chown -R "${USER}" "/home/${USER}/.davfs2"
        chown "${USER}" "/home/${USER}/researchdrive"
        chmod 600 "/home/${USER}/.davfs2/secrets"
        sed -i "/\/home\/${USER}\/researchdrive/d" /etc/fstab
        echo "${URL} /home/${USER}/researchdrive davfs rw,user,uid=${USER},noauto,_netdev,exec 0 0" >> /etc/fstab
    fi
}

# Script start
case "${USER}" in
    root|ubuntu|"" )
        exit 0
    ;;
esac
echo "$(get_credentials)" | awk '{print $1" "$3}'
read -r username password url<<< "$(get_credentials)"
create_secret "${username}" "${password}" "${url}"
