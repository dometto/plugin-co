#!/bin/bash
set -e

# for USER in $(users)
for USER in $(cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1)
do
    /usr/local/bin/researchdrive_credentials.sh "$USER" || true
    /usr/local/bin/researchdrive_mount.sh "$USER" || true
done
