#!/bin/bash
HOME_DIR="/home/${USER}"

umount "${HOME_DIR}/researchdrive" || :
rm "${HOME_DIR}/researchdrive.lock" || :
if [ -f "${HOME_DIR}/.davfs2/secrets" ] && [ "$(stat -c "%a" "${HOME_DIR}/.davfs2/secrets")" == 600 ] && ! mount | grep -q " ${HOME_DIR}/researchdrive"; then
  if [ ! -f "${HOME_DIR}/researchdrive.lock" ]; then
    touch "${HOME_DIR}/researchdrive.lock"
    sudo -n -u "${USER}" mount "${HOME_DIR}/researchdrive"
    rm "${HOME_DIR}/researchdrive.lock" || :
    chmod 700 "${HOME_DIR}/researchdrive"
  fi
fi