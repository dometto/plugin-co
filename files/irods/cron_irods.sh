#!/bin/bash
set -e

for USER in $(cut -d: -f1,3 /etc/passwd | egrep ':[0-9]{4}$' | cut -d: -f1)
do
    /usr/local/bin/irods_credentials.sh "$USER" || true
done
