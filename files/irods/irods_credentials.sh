#!/bin/bash

set -e

# OS dependent parameters
CURL=$(/usr/bin/which curl)
JQ=$(/usr/bin/which jq )
CAT=$(/usr/bin/which cat )
MKDIR=$(/usr/bin/which mkdir )
CUT=$(/usr/bin/which cut)
SED=$(/usr/bin/which sed)
ECHO=$(/usr/bin/which echo)
CHOWN=$(/usr/bin/which chown)
IINIT=$(/usr/bin/which iinit)
CHMOD=$(/usr/bin/which chmod)
MOUNT=$(/usr/bin/which mount)
GREP=$(/usr/bin/which grep)

# User dependent parameters
RC_USERNAME="${1}"

do_log() {
    echo "`date "+%Y.%m.%d-%H:%M:%S %Z"` $@"
}

# get credentials from workspace.json
get_irods_config() {
    set -e
    CO_USER_API_ENDPOINT=$(jq -r .co_user_api_endpoint < /etc/rsc/workspace.json)
    CO_TOKEN=$(jq -r .co_token < /etc/rsc/workspace.json)
    CO_USER_INFO=$(curl --silent -H "Authorization: ${CO_TOKEN}" "${CO_USER_API_ENDPOINT}" | jq -r '.')
    IRODS_CONFIG=$( echo $CO_USER_INFO | ${JQ} --arg USERNAME "${RC_USERNAME}" -r '.[] | select(.username==$USERNAME) | .services | .[0] ')

    if [ "$IRODS_CONFIG" = "null" ]
    then
        exit 1
    fi
    echo ${IRODS_CONFIG}
}


# generates irods environment file and initializes password
configure_icommands () {
    if [ ! -e  $IRODS_ENVIRONMENT_FILE ] || [ ! -e $IRODS_AUTHENTICATION_FILE ]
    then
        do_log ${MKDIR} -p /home/${RC_USERNAME}/.irods/
        ${MKDIR} -p /home/${RC_USERNAME}/.irods/
        do_log "create ${IRODS_ENVIRONMENT_FILE}"
        IRODS_AUTHENTICATION_SCHEME=$(      echo ${IRODS_CONFIG} | $JQ -r '.irods_authentication_scheme // "PAM"' )
        IRODS_ENCRYPTION_NUM_HASH_ROUNDS=$( echo ${IRODS_CONFIG} | $JQ -r '.irods_encryption_num_hash_rounds // "16"' )
        IRODS_CLIENT_SERVER_POLICY=$(       echo ${IRODS_CONFIG} | $JQ -r '.irods_client_server_policy // "CS_NEG_REQUIRE"' )
        IRODS_ENCRYPTION_ALGORITHM=$(       echo ${IRODS_CONFIG} | $JQ -r '.irods_encryption_algorithm // "AES-256-CBC"' )
        IRODS_ENCRYPTION_SALT_SIZE=$(       echo ${IRODS_CONFIG} | $JQ -r '.irods_encryption_salt_size // "8"' )
        IRODS_SSL_VERIFY_SERVER=$(          echo ${IRODS_CONFIG} | $JQ -r '.irods_ssl_verify_server // "hostname"' )
        IRODS_ENCRYPTION_KEY_SIZE=$(        echo ${IRODS_CONFIG} | $JQ -r '.irods_encryption_key_size // "32"' )
        IRODS_PORT=$(                       echo ${IRODS_CONFIG} | $JQ -r '.irods_port // "1247"' )
        ${CAT} <<EOF > ${IRODS_ENVIRONMENT_FILE}
    {
      "irods_host": "$IRODS_URL",
      "irods_port": $IRODS_PORT,
      "irods_user_name": "$IRODS_USERNAME",
      "irods_zone_name": "$IRODS_ZONE",
      "irods_authentication_scheme": "$IRODS_AUTHENTICATION_SCHEME",
      "irods_encryption_num_hash_rounds": $IRODS_ENCRYPTION_NUM_HASH_ROUNDS,
      "irods_client_server_policy": "$IRODS_CLIENT_SERVER_POLICY",
      "irods_encryption_algorithm": "$IRODS_ENCRYPTION_ALGORITHM",
      "irods_encryption_salt_size": $IRODS_ENCRYPTION_SALT_SIZE,
      "irods_ssl_verify_server": "$IRODS_SSL_VERIFY_SERVER",
      "irods_encryption_key_size": $IRODS_ENCRYPTION_KEY_SIZE,
      "irods_client_server_negotiation": "request_server_negotiation"
    }
EOF
        ${CHOWN} -R "${RC_USERNAME}" "/home/${RC_USERNAME}/.irods"
        IRODS_ENVIRONMENT_FILE=$IRODS_ENVIRONMENT_FILE \
                              IRODS_AUTHENTICATION_FILE=$IRODS_AUTHENTICATION_FILE \
                             # sudo -E -u ${RC_USERNAME} ${IINIT} ${IRODS_TOKEN}
	                      sudo -u ${RC_USERNAME} ${IINIT} <<- ANSWERS 
${IRODS_TOKEN} 
ANSWERS
    fi
}

irods_mount () {
    set -e
    RC_USERNAME="$1"
    IRODS_USERNAME="$2"
    IRODS_PASSWORD="$3"
    IRODS_WEBDAV_URL="$4"
    if [ -n "${IRODS_USERNAME}" ] && [ -n "${IRODS_PASSWORD}" ]; then

	do_log ${MKDIR} -p "/home/${RC_USERNAME}/.davfs2/" "/home/${RC_USERNAME}/irods/"
        ${MKDIR} -p "/home/${RC_USERNAME}/.davfs2/" "/home/${RC_USERNAME}/irods/"

	do_log configure /home/${RC_USERNAME}/.davfs2/secrets
        ${ECHO} "${IRODS_WEBDAV_URL} ${IRODS_USERNAME} ${IRODS_PASSWORD}" > "/home/${RC_USERNAME}/.davfs2/secrets"
        ${CHOWN} -R "${RC_USERNAME}" "/home/${RC_USERNAME}/.davfs2"
        ${CHOWN} "${RC_USERNAME}" "/home/${RC_USERNAME}/irods"
        ${CHMOD} 600 "/home/${RC_USERNAME}/.davfs2/secrets"

	do_log configure /etc/fstab "https://${IRODS_WEBDAV_URL} /home/${RC_USERNAME}/irods davfs rw,user,uid=${RC_USERNAME},username=${IRODS_USERNAME},noauto,_netdev 0 0"
        ${GREP} -q "/home/${RC_USERNAME}/irods" /etc/fstab || \
	    echo "https://${IRODS_WEBDAV_URL} /home/${RC_USERNAME}/irods davfs rw,user,uid=${RC_USERNAME},username=${IRODS_USERNAME},noauto,_netdev 0 0" >> /etc/fstab
	echo ${MOUNT} /home/${RC_USERNAME}/irods
        ${MOUNT} /home/${RC_USERNAME}/irods <<- ANSWERS
	${IRODS_PASSWORD}
	ANSWERS
    fi
}

# Script start
case "${1}" in
    rscconfig|root|ubuntu|"" )
        exit 0
    ;;
esac

IRODS_CONFIG=$( get_irods_config )

IRODS_USERNAME=$(   echo ${IRODS_CONFIG} | $JQ -r ' .irods | .username' )
IRODS_URL=$(        echo ${IRODS_CONFIG} | $JQ -r ' .irods | .url ' )
IRODS_TOKEN=$(      echo ${IRODS_CONFIG} | $JQ -r ' .id ' )
IRODS_ZONE=$(       echo ${IRODS_CONFIG} | $JQ -r ' .irods | .zone ' )
IRODS_WEBDAV_URL=$( echo ${IRODS_CONFIG} | $JQ -r ' .irods | .davrods_url ' )
IRODS_ENVIRONMENT_FILE=/home/${RC_USERNAME}/.irods/irods_environment.json
IRODS_AUTHENTICATION_FILE=/home/${RC_USERNAME}/.irods/.irodsA
IRODS_WEBDAV_PORT=$(  echo ${IRODS_CONFIG} | $JQ -r ' .irods | .davrods_port // "443"' )

# configure icommands
configure_icommands

# configure davfs
irods_mount ${RC_USERNAME} ${IRODS_USERNAME} ${IRODS_TOKEN} ${IRODS_WEBDAV_URL}
